import React, { useState, useEffect } from 'react';
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  Form,
  FormGroup,
  Label,
  Input
} from 'reactstrap';
import { URL } from "../config";

const MemberImgModal = ({ imgModal, toggle, member }) => {
  const [profilePic, setProfilePic] = useState('');

  //so that initial values will be replaced after inital load, and whenver toggle is called
  useEffect(() => {
    setProfilePic(member.profilePic);
  }, [toggle]);

  //UPDATE A MEMBER
  const onChangeHandler = e => {
    setProfilePic({
      [e.target.name]: e.target.value
    });
  };

  const onSubmitHandler = async e => {
    e.preventDefault();

    const updates = {
      //   teamId,
      //   position
    };

    // updateMember(member._id, updates);
  };

  return (
    <Modal isOpen={imgModal} toggle={toggle}>
      <ModalHeader toggle={toggle}>Update Profile Pic</ModalHeader>
      <ModalBody>
        <Form className='p-4 border rounded' onSubmit={e => onSubmitHandler(e)}>
          <img
            src={`${URL}/members/${member._id}/upload`}
            alt=''
            className='img-fluid'
          />
          <FormGroup>
            <Input
              type='file'
              name='profilePic'
              id='profilePic'
              value={member.profilePic}
              onChange
            />
          </FormGroup>

          <br />
          <Button color='primary' className='btn-block'>
            Save Changes
          </Button>
        </Form>
      </ModalBody>
    </Modal>
  );
};

export default MemberImgModal;
