import React, { useState, useEffect } from 'react';
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  Form,
  FormGroup,
  Label,
  Input,
  FormText
} from 'reactstrap';
import axios from 'axios';

const MemberModal = ({ modal, toggle, member, teams, updateMember }) => {
  const [setMember, setMemberData] = useState({
    teamId: '',
    position: '',
    teams: []
  });

  const { teamId, position } = setMember;

  //so that initial values will be replaced after inital load, and whenver toggle is called
  useEffect(() => {
    setMemberData({
      teamId: member.teamId ? member.teamId._id : null,
      position: member.position ? member.position : null,
      teams: teams
    });

    // setLoading(false)
  }, [toggle]);


  //POPULATE TEAMS DROPDOWN
  const populateTeams = () => {
    return teams.map(team => {
      return (
        <option
          key={team._id}
          value={team._id}
          selected={
            teamId === null ? false : teamId === team._id ? true : false
          }
        >
          {team.name}
        </option>
      );
    });
  };
  //IF NOT TEAM
  let NA;
  if (teamId == null) {
    NA = (
      <option selected disabled>
        No Team Yet....
      </option>
    );
  }

  //UPDATE A MEMBER
  const onChangeHandler = e => {
    setMemberData({
      ...setMember,
      [e.target.name]: e.target.value
    });
  };

  const onSubmitHandler = async e => {
    e.preventDefault();

    const updates = {
      teamId,
      position
    };

    updateMember(member._id, updates);
  };

  return (
    <Modal isOpen={modal} toggle={toggle}>
      <ModalHeader toggle={toggle}>Update Member</ModalHeader>
      <ModalBody>
        <Form className='p-4 border rounded' onSubmit={e => onSubmitHandler(e)}>
          <FormGroup>
            <Label for='username'>Username</Label>
            <Input
              type='text'
              name='username'
              id='username'
              value={member.username}
              disabled
            />
          </FormGroup>
          <FormGroup>
            <Label for='position'>Team</Label>
            <Input
              type='select'
              name='teamId'
              id='teamId'
              onChange={e => onChangeHandler(e)}
            >
              {NA}
              {populateTeams()}
            </Input>
          </FormGroup>
          <FormGroup>
            <Label for='position'>Position</Label>
            <Input
              type='select'
              name='position'
              id='position'
              onChange={e => onChangeHandler(e)}
            >
              <option
                value='admin'
                selected={position === 'admin' ? true : false}
              >
                Admin
              </option>
              <option value='ca' selected={position === 'ca' ? true : false}>
                CA
              </option>
              <option
                value='instructor'
                selected={position === 'instructor' ? true : false}
              >
                Instructor
              </option>
              <option value='hr' selected={position === 'hr' ? true : false}>
                HR
              </option>
              <option
                value='student'
                selected={position === 'student' ? true : false}
              >
                Student
              </option>
            </Input>
          </FormGroup>

          <br />
          <Button color='primary' className='btn-block'>
            Save Changes
          </Button>
        </Form>
      </ModalBody>
    </Modal>
  );
};

export default MemberModal;
