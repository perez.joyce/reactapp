import React, { useState, useEffect } from 'react';
import { Link, Redirect, useHistory } from 'react-router-dom';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import axios from 'axios';
import Swal from 'sweetalert2';
import { URL } from "../config";

const LoginForm = props => {
  // console.log(props);

  //   DEMO
  //   console.log(useState())
  //   console.log(useState('hello'))
  //   const [username, setUsername] = useState('hello');
  //   console.log(username);

  /*=====================
    3) HOOK
    ...in the past, function components used to be stateless
    A Hook is a special function that lets you “hook into” React features without writing a class.
    
    For example, useState is a Hook that lets you add React state to function components. 

    4) STATE OF A COMPONENT
    - an object that holds a component's dynamic data and determines the component's behavior
    - enables a component to keep track of changing information in between renders

    5) USESTATE
    console.log(useState())
    console.log(useState("initial value"))
    The only argument to the useState() Hook is the initial state.
    It returns a pair of values: the current state and a function that updates it.

    Associate to input field by adding value (username which is state) and event (i.e., onChange)

    WHY is there useState()?
    If you write a function component and realize you need to add some state to it, previously you had to convert it to a class. 
    Now you can use a Hook inside the existing function component. 

    =========================*/
  const [formData, setFormData] = useState({
    email: 'joyce1234@gmail.com',
    password: 'joyce1234'
  });


  //  DESTRUCTURE so you can access individual keys as variables
  const { email, password } = formData;
  //  console.log(email);


  const [isRedirected, setIsRedirected] = useState(false);

  const onChangeHandler = e => {
    // console.log(e);
    // console.log(e.target);
    // //Backticks can use multi-line strings and string interpolation features with
    // console.log(`e.target.name: ${e.target.name}`);
    // console.log(`e.target.value: ${e.target.value}`);

    //set the state
    setFormData({
      ...formData,
      [e.target.name]: e.target.value
    });
  };

  //NOT async if we're not going to use axios
  const onSubmitHandler = async e => {
    e.preventDefault();
    props.setLoading(true)

    const member = {
      email,
      password
    };

    try {

      const config = {
        headers: {
          'Content-Type': 'application/json'
        }
      };

      //recreate config
      //signify because you can't pass object to URL
      const body = JSON.stringify(member);

      const res = await axios.post(
        `${URL}/members/login`,
        body,
        config
      )

      //set token
      localStorage.setItem('username', res.data.member.username);
      localStorage.setItem('token', res.data.token);
      //redirect to login
      window.location = "/profile"
    } catch (error) {
      //remove token if there is
      localStorage.removeItem('token');

      // console.log(error.response.data);

      // console.log(error.response.data.message);
      // Swal.fire({
      //   title: 'Error!',
      //   text: error.response.data.message,
      //   icon: 'error',
      //   showConfirmButton: false,
      //   timer: 1500
      // })
    }
  };

  const [disabled, setDisabled] = useState(true);

  //USE EFFECT
  /*
    A hook that tells React that your component needs to do something after render. 
    Placing useEffect inside the component lets us access the count state variable (or any props) right from the effect
    By default, it runs both AFTER the first render and after every update
  */
  useEffect(() => {
    // console.log(formData)
    if (email !== '' && password !== '') {
      setDisabled(false);
    } else {
      setDisabled(true);
    }
  }, [formData]);


  if (isRedirected) {
    // return <Redirect to="/profile"/>
    return window.location = '/profile';
  }


  //regex+ one or more occurrences of the preceding element; For example, ab+c matches "abc", "abbc", "abbbc", and so on, but not "ac".
  return (

    <Form className='p-4 border rounded' onSubmit={e => onSubmitHandler(e)}>

      <FormGroup>
        <Label for='email'>Email</Label>
        <Input
          type='email'
          name='email'
          id='email'
          value={email}
          onChange={e => onChangeHandler(e)}
          required
        />
      </FormGroup>
      <FormGroup>
        <Label for='password'>Password</Label>
        <Input
          type='password'
          name='password'
          id='password'
          value={password}
          onChange={e => onChangeHandler(e)}
          required
          minLength='5'
        />
        <FormText color='muted'>Must have at least 5 characters.</FormText>
      </FormGroup>

      <br />
      <Button color='primary' className='btn-block mb-3' disabled={disabled}>
        LOGIN
      </Button>
      <p>
        Already have an account?
        <Link to='/login' className='font-weight-bold'>
          Login
        </Link>
      </p>
    </Form>

  );
};

export default LoginForm;
