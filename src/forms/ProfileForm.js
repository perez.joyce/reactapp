import React, { useState, useEffect } from 'react';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import axios from 'axios';
import { getProfile } from '../rest/queries/members';

const ProfileForm = props => {
  console.log('ProfileForm props', props);

  const { firstName, lastName, username, position, age, email } = props.member;

  const onChangeHandler = e => {
    //set the state
    props.setMemberData({
      ...props.member,
      [e.target.name]: e.target.value
    });
  };

  const onSubmitHandler = e => {
    e.preventDefault();

    return props.updateProfile(props.member);
  };

  return (
    <Form className='p-4 border rounded' onSubmit={e => onSubmitHandler(e)}>
      <FormGroup>
        <Label for='firstName'>First Name</Label>
        <Input
          type='text'
          name='firstName'
          id='firstName'
          value={firstName}
          onChange={e => onChangeHandler(e)}
        />
      </FormGroup>
      <FormGroup>
        <Label for='lastName'>Last Name</Label>
        <Input
          type='text'
          name='lastName'
          id='lastName'
          value={lastName}
          onChange={e => onChangeHandler(e)}
        />
      </FormGroup>
      <FormGroup>
        <Label for='username'>Username</Label>
        <Input
          type='text'
          name='username'
          id='username'
          value={username}
          onChange={e => onChangeHandler(e)}
          required
        />
      </FormGroup>
      <FormGroup>
        <Label for='email'>Email</Label>
        <Input
          type='email'
          name='email'
          id='email'
          value={email}
          onChange={e => onChangeHandler(e)}
          required
        />
      </FormGroup>
      <FormGroup>
        <Label for='position'>Position</Label>
        <Input
          type='text'
          name='position'
          id='position'
          value={position}
          onChange={e => onChangeHandler(e)}
        />
      </FormGroup>
      <FormGroup>
        <Label for='age'>Age</Label>
        <Input
          type='number'
          name='age'
          id='age'
          value={age}
          onChange={e => onChangeHandler(e)}
        />
      </FormGroup>

      <br />
      <Button color='primary' className='btn-block'>
        Save Changes
      </Button>
    </Form>
  );
};

export default ProfileForm;
