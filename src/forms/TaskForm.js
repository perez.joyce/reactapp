import React from 'react';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';

const Example = props => {
  return (
    <Form className='p-4 border rounded'>
      <FormGroup>
        <Label for='username'>Name</Label>
        <Input type='text' name='username' id='username' disabled />
      </FormGroup>
      <br />
      <Button color='primary' className='btn-block'>
        Save Changes
      </Button>
    </Form>
  );
};

export default Example;
