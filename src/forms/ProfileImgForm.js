import React, { useState, useEffect } from 'react';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import axios from 'axios';
import { getProfile } from '../rest/queries/members';

const ProfileImgForm = props => {
  // console.log('ProfileImg props', props);

  const [profilePic, setProfilePic] = useState(undefined);

  // const image = (
  //   <img
  //     src={`http://localhost:4000/members/${props._id}/upload`}
  //     alt=''
  //     className='img-fluid'
  //   />
  // );

  const onChangeHandler = e => {
    //set the state
    setProfilePic(e.target.files[0]);
  };

  const onSubmitHandler = e => {
    e.preventDefault();
    const formData = new FormData();
    formData.append('upload', profilePic);
    props.updateImage(formData);
  };

  return (
    <Form className='p-4 border rounded' onSubmit={e => onSubmitHandler(e)}>
      <FormGroup>
        <img
          src={`http://localhost:4000/members/${props._id}/upload`}
          alt=''
          className='img-fluid'
        />
        <Input
          type='file'
          name='profilePic'
          id='profilePic'
          onChange={e => onChangeHandler(e)}
        />
      </FormGroup>
      <br />
      <Button className='btn-block bg-secondary border'>Save Image</Button>
    </Form>
  );
};

export default ProfileImgForm;
