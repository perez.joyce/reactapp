import React, { useState, useEffect } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import axios from 'axios';
import Swal from 'sweetalert2';
// import { useAlert, types } from 'react-alert'

const SignupForm = props => {
  
  //   DEMO
  //   console.log(useState())
  //   console.log(useState('hello'))
  //   const [username, setUsername] = useState('hello');
  //   console.log(username);

  /*=====================
    3) HOOK
    ...in the past, function components used to be stateless
    A Hook is a special function that lets you “hook into” React features without writing a class.
    
    For example, useState is a Hook that lets you add React state to function components. 

    4) STATE OF A COMPONENT
    - an object that holds a component's dynamic data and determines the component's behavior
    - enables a component to keep track of changing information in between renders

    5) USESTATE
    console.log(useState())
    console.log(useState("initial value"))
    The only argument to the useState() Hook is the initial state.
    It returns a pair of values: the current state and a function that updates it.

    Associate to input field by adding value (username which is state) and event (i.e., onChange)

    WHY is there useState()?
    If you write a function component and realize you need to add some state to it, previously you had to convert it to a class. 
    Now you can use a Hook inside the existing function component. 

    =========================*/
  // const alert = useAlert();
  const [formData, setFormData] = useState({
    username: '',
    email: '',
    password: '',
    password2: ''
  });

  //  console.log(formData.email);
  //  DESTRUCTURE so you can access individual keys as variables
  const { username, email, password, password2 } = formData;
  //  console.log(email);

  const [isRedirected, setIsRedirected] = useState(false)

  const onChangeHandler = e => {
    
    console.log(e)
    console.log(e.target)
    //Backticks can use multi-line strings and string interpolation features with 
    console.log(`e.target.name: ${e.target.name}`);
    console.log(`e.target.value: ${e.target.value}`);

    //set the state
    setFormData({
      ...formData,
      [e.target.name]: e.target.value
    });
  };

  //NOT async if we're not going to use axios
  const onSubmitHandler = async e => {
    e.preventDefault();
    // console.log(formData);
    if (password !== password2) {
      console.log('Passwords don\'t match');


  
      //npm i sweetalert2
      // Swal.fire({
      //     title: 'Error!', 
      //     text: 'Passwords don\'t match!',
      //     icon: 'error',
      //     showConfirmButton: false,
      //     timer: 1500
      //   })

      //npm install --save react-alert react-alert-template-basic
      // alert.show('Passwords don\'t match', {
      //   type: 'error'
      // })
      // alert.show(<div style={{ color: 'blue' }}>Password don't match</div>)


    } else {
      //   console.log(formData);

      //TEST TO SEE IF WORKING
      /*
      frontend
      - npm i axios 

      backend
      - npm i cors
      - app.use(cors())
      */
      const newMember = {
        username,
        email,
        password
      };

      try {
        const config = {
          headers: {
            'Content-Type': 'application/json'
          }
        };

        //recreate config
        //signify because you can't pass object to URL
        const body = JSON.stringify(newMember);

        const res = await axios.post(
          'http://localhost:4000/members',
          body,
          config
        );
        console.log(res);

        //get token
        console.log(res.data);

        //redirect to login
        setIsRedirected(true)

      } catch (error) {
        console.log(error.response.data)


        console.log(error.response.data.message)
        // Swal.fire({
        //   title: 'Error!', 
        //   text: error.response.data.message,
        //   icon: 'error',
        //   showConfirmButton: false,
        //   timer: 1500
        // })
      }
    }
  }

  const [disabled, setDisabled] = useState(true)
  // INFINITE LOOP
  // if(username !== "" && email !== "" && password !== "" && password2 !== "") {
  //     setDisabled(false)
  //   } 
  //   else {
  //     setDisabled(true)
  //   }
  

  //USE EFFECT
  /*
    A hook that tells React that your component needs to do something after render. 
    Placing useEffect inside the component lets us access the count state variable (or any props) right from the effect
    By default, it runs both AFTER the first render and after every update
  */
  useEffect(() => {
    // console.log(formData)
    if(username !== "" && email !== "" && password !== "" && password2 !== "") {
      setDisabled(false)
    } else {
      setDisabled(true)
    }
  }, [formData]);
  
  
  if(isRedirected){
    return <Redirect to="/login"/>
  }

  // useEffect(() => {
  //   if(isRedirected){
  //     <Redirect to="/login"/>
  //   }
  // }, [isRedirected])


//regex+ one or more occurrences of the preceding element; For example, ab+c matches "abc", "abbc", "abbbc", and so on, but not "ac".
  return (
    <Form className='p-4 border rounded' onSubmit={e => onSubmitHandler(e)}>
      <FormGroup>
        <Label for='username'>Username</Label>
        <Input
          type='text'
          name='username'
          id='username'
          value={username}
          onChange={e => onChangeHandler(e)}
          required
          maxLength="30"
          pattern="[a-zA-Z0-9]+"
        />
        <FormText color="muted">
          Use alphanumeric characteres only.
        </FormText>
      </FormGroup>
      <FormGroup>
        <Label for='email'>Email</Label>
        <Input
          type='email'
          name='email'
          id='email'
          value={email}
          onChange={e => onChangeHandler(e)}
          required
        />
      </FormGroup>
      <FormGroup>
        <Label for='password'>Password</Label>
        <Input
          type='password'
          name='password'
          id='password'
          value={password}
          onChange={e => onChangeHandler(e)}
          required
          minLength="5"
        />
        <FormText color="muted">
          Must have at least 5 characters.
        </FormText>
      </FormGroup>
      <FormGroup>
        <Label for='password2'>Confirm Password</Label>
        <Input
          type='password'
          name='password2'
          id='password2'
          value={password2}
          onChange={e => onChangeHandler(e)}
          required
        />
      </FormGroup>

      <br />
      <Button color='primary' className='btn-block mb-3' disabled={disabled}>
        Register
      </Button>
      <p>
        Already have an account?
        <Link to='/login' className='font-weight-bold'>
          Sign In
        </Link>
      </p>
    </Form>
  );
}



/*
ACTIVITY:
Do the same thing for login.
Except login only asks you for username and password

*/

export default SignupForm;
