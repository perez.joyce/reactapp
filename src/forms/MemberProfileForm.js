import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import axios from 'axios';

const MemberProfileForm = props => {
  // console.log('Member Profile Form', props.memberAttr);

  return (
    <Fragment>
      <img
        src={`http://localhost:4000/members/${props.memberAttr._id}/upload`}
        alt=''
        className='img-fluid'
      />
      <Form className='p-4 border rounded'>
        <FormGroup>
          <Label for='username'>Username</Label>
          <Input type='text' name='username' id='username' disabled />
        </FormGroup>
        <FormGroup>
          <Label for='email'>Email</Label>
          <Input type='email' name='email' id='email' disabled />
        </FormGroup>
        <FormGroup>
          <Label for='position'>Team</Label>
          <Input type='select' name='select' id='position'>
            <option selected>1</option>
            <option>2</option>
            <option>3</option>
            <option>4</option>
          </Input>
        </FormGroup>
        <FormGroup>
          <Label for='position'>Position</Label>
          <Input type='select' name='select' id='position'>
            <option selected>Admin</option>
            <option>CA</option>
            <option>Instructor</option>
            <option>HR</option>
          </Input>
        </FormGroup>

        <br />
        <Button color='primary' className='btn-block'>
          Save Changes
        </Button>
      </Form>
      <br />
      <Link className='btn btn-block btn-secondary' to='/members'>
        Back
      </Link>
    </Fragment>
  );
};

export default MemberProfileForm;
