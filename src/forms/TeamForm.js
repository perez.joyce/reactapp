import React from 'react';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';

const TeamForm = props => {
  return (
    <Form className='p-4 border rounded'>
      <FormGroup>
        <Label for='name'>Name</Label>
        <Input type='text' name='name' id='name' required />
      </FormGroup>

      <br />
      <Button color='primary' className='btn-block'>
        Save Changes
      </Button>
    </Form>
  );
};

export default TeamForm;
