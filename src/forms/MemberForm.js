import React from 'react';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';

const MemberForm = props => {
 
  return (
    <Form className='p-4 border rounded'>
      <FormGroup>
        <Label for='username'>Username</Label>
        <Input type='text' name='username' id='username' disabled />
      </FormGroup>
      <FormGroup>
        <Label for='email'>Email</Label>
        <Input type='email' name='email' id='email' disabled />
      </FormGroup>
      <FormGroup>
        <Label for='position'>Team</Label>
        <Input type='select' name='select' id='position'>
          <option selected>1</option>
          <option>2</option>
          <option>3</option>
          <option>4</option>
        </Input>
      </FormGroup>
      <FormGroup>
        <Label for='position'>Position</Label>
        <Input type='select' name='select' id='position'>
          <option selected>Admin</option>
          <option>CA</option>
          <option>Instructor</option>
          <option>HR</option>
        </Input>
      </FormGroup>

      <br />
      <Button color='primary' className='btn-block'>
        Save Changes
      </Button>
    </Form>
  );
};

export default MemberForm;
