//declare dependencies
//destructuring
import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Button } from 'reactstrap';
import './style.css';
import { transitions, positions, Provider as AlertProvider } from 'react-alert'
import AlertTemplate from 'react-alert-template-basic'
import setAuthToken from './utils/setAuthToken'

//components
import AppNavbar from './partials/AppNavbar';
import MembersPage from './pages/MembersPage';
import TeamsPage from './pages/TeamsPage';
import TasksPage from './pages/TasksPage';
import LoginPage from './pages/LoginPage';
import SignupPage from './pages/SignupPage';
import MainPage from './pages/MainPage';
import NotFoundPage from './pages/NotFoundPage';

//define the root
const root = document.querySelector('#root');

//create component
// const pageComponent = (
// 	<Fragment>
// 		<h1>Hello, Batch 44!</h1>
// 		{/*<button className="bg-pink">Pink</button>*/}
// 		<Button color="primary" className="mr-1">Primary</Button>
// 		<Button className="bg-pink">Pink</Button>
// 	</Fragment>
// 	)

// optional cofiguration
const options = {
  // you can also just use 'bottom center'
  position: positions.TOP_RIGHT,
  timeout: 5000,
  // offset: '30px',
  // you can also just use 'scale'
  transition: transitions.SCALE
}


if(localStorage.token) {
  setAuthToken(localStorage.token)
}



const [appData, setAppData ] = useState({
  email: null,
  passwrod: null,
  login: false
})

const { email, password, login } = appData;



const pageComponent = (
<AlertProvider template={AlertTemplate} {...options}>
  <BrowserRouter>
    <AppNavbar />
    <Switch>
      <Route component={MembersPage} path='/members' />
      <Route component={TeamsPage} path='/teams' />
      <Route component={TasksPage} path='/tasks' />
      <Route component={LoginPage} path='/login' />
      <Route component={SignupPage} path='/signup' />
      <Route component={MainPage} exact path='/' />
      <Route component={NotFoundPage} path='*' />
    </Switch>
  </BrowserRouter>
</AlertProvider>
)

//render component
ReactDOM.render(pageComponent, root);
