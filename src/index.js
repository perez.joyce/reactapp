//declare dependencies
//destructuring
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

//define the root
const root = document.querySelector('#root');


const pageComponent = <App />

//render component
ReactDOM.render(pageComponent, root);
