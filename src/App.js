//declare dependencies
//destructuring
import React, { useState, useEffect } from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"
import './style.css';

//main components
import AppNavbar from './partials/AppNavbar';
import MembersPage from './pages/MembersPage';
import MemberProfilePage from './pages/MemberProfilePage';
import TeamsPage from './pages/TeamsPage';
import TasksPage from './pages/TasksPage';
import ProfilePage from './pages/ProfilePage';
import LoginPage from './pages/LoginPage';
import SignupPage from './pages/SignupPage';
import MainPage from './pages/MainPage';
import NotFoundPage from './pages/NotFoundPage';
import ProtectedRoute from './auth/ProtectedRoute';

const App = () => {
  const [appData, setAppData] = useState({
    username: localStorage.username,
    token: localStorage.token
  });

  const { username, token } = appData;
  // const { username, token } = appData;
  // console.log('App Token', token);
  // console.log('App Username', username);

  const currentUser = () => {
    return { username, token };
  };

  const Load = (props, page) => {
    if (page === 'MainPage')
      return <MainPage {...props} getCurrentUser={currentUser()} />;

    if (token && page === 'SignupPage') {
      // console.log('Signup Page IF');
      return <Redirect to='/profile' />;
    } else if (page === 'SingupPage') {
      // console.log('Signup Page ELSE');
      return <SignupPage {...props} />;
    }

    if (token && page === 'LoginPage') {
      // console.log('Login Page IF');
      return <Redirect to='/profile' />;
    } else if (page === 'LoginPage') {
      // console.log('Login Page ELSE');
      return <LoginPage {...props} />;
    }

    if (!token) return <Redirect to='/login' />;

    if (page === 'LogoutPage') {
      localStorage.clear();
      setAppData({
        token,
        username
      });
      return (window.location = '/login');
    }

    switch (page) {
      // case 'LoginPage':
      //   return <LoginPage {...props} token={token} />;
      case 'MemberProfilePage':
        return <MemberProfilePage {...props} getCurrentUser={currentUser()} />;
      case 'MembersPage':
        return <MembersPage {...props} getCurrentUser={currentUser()} />;
      case 'TeamsPage':
        return <TeamsPage {...props} getCurrentUser={currentUser()} />;
      case 'TasksPage':
        return <TasksPage {...props} getCurrentUser={currentUser()} />;
      case 'ProfilePage':
        return <ProfilePage {...props} token={token} />;
      default:
        return <NotFoundPage />;
    }
  };

  /*
  RENDER prop
  - a technique for sharing code between react components
  - takes in a function that returns a react component with props
  - expects component declaration
  */

  return (
    <BrowserRouter>
      <AppNavbar username={username} token={token} />
      <Switch>
        <Route path='/login' render={props => Load(props, 'LoginPage')} />
        <Route path='/signup' render={props => Load(props, 'SignupPage')} />
        <Route path='/profile' render={props => Load(props, 'ProfilePage')} />
        <Route
          exact
          path='/members/:id'
          render={props => Load(props, 'MemberProfilePage')}
        />
        <Route path='/members' render={props => Load(props, 'MembersPage')} />

        <Route path='/teams' render={props => Load(props, 'TeamsPage')} />
        <Route path='/tasks' render={props => Load(props, 'TasksPage')} />
        <Route path='/logout' render={props => Load(props, 'LogoutPage')} />
        <Route component={MainPage} exact path='/' />
        <Route component={NotFoundPage} path='*' />
      </Switch>
    </BrowserRouter>
  );
};

//render component
export default App;
