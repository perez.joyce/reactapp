import React, { useState, Fragment } from 'react';
import { Link } from 'react-router-dom';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavbarText
} from 'reactstrap';

const AppNavbar = props => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  // console.log("username", localStorage.username)
  // console.log("username", props.username)

  let authLinks;
  let navLinks;

  if (!props.token) {
    authLinks = (
      <Fragment>
        <Link className='btn btn-primary' to='/login'>
          Login
        </Link>
        <Link className='btn btn-secondary ml-3' to='/signup'>
          Register
        </Link>
      </Fragment>
    );
  } else {
    navLinks = (
      <Fragment>
        <NavItem>
          {/*<NavLink href='/components/'>Members</NavLink>*/}
          <Link to='/members' className='nav-link'>
            Members
          </Link>
        </NavItem>
        <NavItem>
          <Link to='/teams' className='nav-link'>
            Teams
          </Link>
        </NavItem>
        <NavItem>
          <Link to='/tasks' className='nav-link'>
            Tasks
          </Link>
        </NavItem>
      </Fragment>
    );
    authLinks = (
      <Fragment>
        <Link className='ml-3 nav-link text-light' to='/profile'>
          Welcome, {props.username}
        </Link>
        <Link className='btn btn-secondary ml-3' to='/logout'>
          Logout
        </Link>
      </Fragment>
    );
  }

  return (
    <div>
      <Navbar color='dark' dark expand='md'>
        <NavbarBrand className='font-weight-bold' href='/'>
          MERN Tracker
        </NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className='mr-auto' navbar>
            {navLinks}
          </Nav>
          {authLinks}
        </Collapse>
      </Navbar>
    </div>
  );
};

export default AppNavbar;
