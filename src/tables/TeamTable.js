import React from 'react';
import { Table } from 'reactstrap';
import TeamRow from '../rows/TeamRow';

const TeamTable = props => {
  // console.log('TeamTable props', props.teamsAttr);

  let row;
  if (props.teamsAttr) {
    let i = 0;
    row = props.teamsAttr.map(team => {
      return <TeamRow team={team} key={team._id} index={++i} />;
    });
  } else {
    row = (
      <tr>
        <td colSpan='4'>
          <em>No teams found.</em>
        </td>
      </tr>
    );
  }
  return (
    <Table size='sm' responsive borderless hover className='p-4 rounded'>
      <thead>
        <tr>
          <th>#</th>
          <th>Name</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>{row}</tbody>
    </Table>
  );
};

export default TeamTable;
