import React from 'react';
import { Table } from 'reactstrap';
import MemberRow from '../rows/MemberRow';

const MemberTable = props => {
  // console.log('Member Table', props);

  let row;
  if (props.membersAttr) {
    let i = 0;
    row = props.membersAttr.map(member => {
      return (
        <MemberRow
          member={member}
          key={member._id}
          index={++i}
          deleteMember={props.deleteMember}
          toggle={props.toggle}
        />
      );
    });
  } else {
    row = (
      <tr>
        <td colSpan='5'>
          <em>No members found.</em>
        </td>
      </tr>
    );
  }
  return (
    <Table size='sm' responsive borderless hover className='p-4 rounded'>
      <thead>
        <tr>
          <th>#</th>
          <th>Username</th>
          <th>Team</th>
          <th>Position</th>
          <th>Status</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>{row}</tbody>
    </Table>
  );
};

export default MemberTable;
