import React from 'react';
import { Button } from 'reactstrap';

const TeamRow = props => {
  // console.log('TeamRow', props.team);
  const { name, index, key } = props.team;

  return (
    <tr key={key}>
      <th scope='row'>{index}</th>
      <td>{name}</td>
      <td className='d-flex flex-row'>
        <Button color='info' className='mr-1'>
          <i className='fas fa-eye'></i>
        </Button>
        <Button color='warning' className='mr-1'>
          <i className='fas fa-edit'></i>
        </Button>
        <Button color='danger' className='mr-1'>
          <i className='fas fa-trash-alt'></i>
        </Button>
      </td>
    </tr>
  );
};

export default TeamRow;
