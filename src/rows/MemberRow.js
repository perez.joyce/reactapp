import React, { Fragment, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Button, ButtonToggle } from 'reactstrap';

const MemberRow = ({ member, index, deleteMember, toggle }) => {
  // console.log(props);
  const { username, teamId, position, isActive, _id } = member;
  let deleteBtn = '';

  if (isActive) {
    deleteBtn = (
      <Button
        color='danger'
        className='mr-1 btn-sm'
        onClick={() => deleteMember(_id)}
      >
        <i className='fas fa-trash-alt'></i>
      </Button>
    );
  }

  return (
    <tr>
      <th scope='row'>{index}</th>
      <td>{username}</td>
      <td>{teamId ? teamId.name : 'N/A'}</td>
      <td>{position}</td>
      <td>{isActive ? 'Active' : 'Deactivated'}</td>
      <td className='d-flex flex-row'>
        <Link className='mr-1 btn btn-info btn-sm' to={`/members/${_id}`}>
          <i className='fas fa-eye'></i>
        </Link>
        <Button
          color='warning'
          className='mr-1 btn-sm'
          onClick={() => toggle(member._id)}
        >
          <i className='fas fa-edit'></i>
        </Button>

        {deleteBtn}
      </td>
    </tr>
  );
};

export default MemberRow;
