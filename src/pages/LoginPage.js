import React, { Fragment, useState, useEffect } from 'react';
// import { useHistory } from 'react-router-dom';
import { Container, Row, Col } from 'reactstrap';
import Loader from 'react-loader-spinner'
import LoginForm from '../forms/LoginForm';
import testImage from '../images/image.jpg'
/*
 1) COMPONENT
    - building blocks of any React app
    - splits UI into independent, reusable pieces and deal with each piece in isolation
    - JS class or function that accepts inputs (i.e., props) and returns JSX  that describes how a UI will appear.
    

  2) PROPS
    - arguments passed into React components via HTML attributes
*/

const LoginPage = props => {
  // console.log('login page', props);
  // const history = useHistory();

  // if (props.token) {
  //   return history.goBack();
  // }

  const [loading, setLoading] = useState(false);

  // if (loading) {
  //   return (
  //     <Container>
  //       <Row>
  //         <Col id="loaderContainer">
  //           <div id="loader">
  //             <Loader
  //               type="BallTriangle"
  //               color="#00BFFF"
  //               height={200}
  //               width={200}
  //             />
  //           </div>

  //         </Col>
  //       </Row>
  //     </Container>
  //   )
  // }
  // const activateLoader = (val) => setLoading(val);

  return (
    <Fragment>
      {loading ? (
        <Container>
          <Row>
            <Col id="loaderContainer">
              <div id="loader">
                <Loader
                  type="BallTriangle"
                  color="#00BFFF"
                  height={200}
                  width={200}
                />
              </div>

            </Col>
          </Row>
        </Container>
      ) : (
          <Container className='my-5' fluid={true}>
            <img src={testImage} alt="" className="img-fluid" />
            <Row className="border">

              <Col md='6'>
                <h1>Login Page</h1>
                <LoginForm setLoading={setLoading} />
              </Col>
            </Row>
          </Container>
        )}

    </Fragment>
  );
};

export default LoginPage;
