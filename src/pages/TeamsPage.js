import React, { useState, useEffect } from 'react';
import { Container, Row, Col } from 'reactstrap';
import TeamForm from '../forms/TeamForm';
import TeamTable from '../tables/TeamTable';
import axios from 'axios';
import { URL } from "../config";

const TeamsPage = props => {
  // console.log(props);

  const [teamsData, setTeamsData] = useState({
    token: props.getCurrentUser.token,
    teams: []
  });

  const { token, teams } = teamsData;
  // console.log('Teams', teams);

  const getTeams = async () => {
    try {
      const config = {
        headers: {
          Authorization: `Bearer ${token}`
        }
      };

      const res = await axios.get(`${URL}/teams`, config);
      // console.log('TeamsPage response', res);

      // //return updated states
      return setTeamsData({
        teams: res.data
      });
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    // getProfile()
    getTeams();
  }, [setTeamsData]);

  return (
    <Container className='my-5' fluid={false}>
      <Row className='mb-3'>
        <Col>
          <h1>Teams</h1>
        </Col>
      </Row>
      <Row>
        <Col md='4' className=''>
          <TeamForm />
        </Col>
        <Col className=''>
          <TeamTable teamsAttr={teams} />
        </Col>
      </Row>
    </Container>
  );
};

export default TeamsPage;
