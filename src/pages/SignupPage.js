import React from 'react';
import { Container, Row, Col } from 'reactstrap';
import SignupForm from '../forms/SignupForm';

/*
 1) COMPONENT
    - building blocks of any React app
    - splits UI into independent, reusable pieces and deal with each piece in isolation
    - JS class or function that accepts inputs (i.e., props) and returns JSX  that describes how a UI will appear.
    

  2) PROPS
    - arguments passed into React components via HTML attributes
*/

const SignupPage = props => {
  return (
    <Container className='my-5' fluid={false}>
      <Row className='mb-3'>
        <Col>
          <h1>Sign Up Page</h1>
        </Col>
      </Row>
      <Row>
        <Col md='6'>
          <SignupForm />
        </Col>
      </Row>
    </Container>
  );
};

export default SignupPage;
