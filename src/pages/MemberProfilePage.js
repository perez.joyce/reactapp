import React, { useState, useEffect } from 'react';
import { Container, Row, Col } from 'reactstrap';
import MemberProfileForm from '../forms/MemberProfileForm';
import axios from 'axios';
import { URL } from "../config";

const MemberProfilePage = props => {
  // console.log(props.match.params.id);
  const [memberData, setMemberData] = useState({
    token: props.getCurrentUser.token,
    member: {}
  });

  const { member, token } = memberData;
  // console.log('MemberProfile Page', member);

  const getMember = async () => {
    try {
      const config = {
        headers: {
          Authorization: `Bearer ${token}`
        }
      };

      const res = await axios.get(
        `${URL}/members/${props.match.params.id}`,
        config
      );
      // console.log(res);

      setMemberData({
        member: res.data
      });
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    getMember();
  }, [setMemberData]);

  return (
    <Container className='my-5' fluid={false}>
      <Row className='mb-3'>
        <Col>
          <h1>Member Profile</h1>
        </Col>
      </Row>
      <Row>
        <Col lg='4' className='mb-5'>
          <MemberProfileForm memberAttr={member} />
        </Col>
      </Row>
    </Container>
  );
};

export default MemberProfilePage;
