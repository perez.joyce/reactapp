import React, { useState, useEffect, Fragment } from 'react';
import {
  Container,
  Row,
  Col,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter
} from 'reactstrap';
import MemberForm from '../forms/MemberForm';
import MemberTable from '../tables/MemberTable';
import MemberModal from '../modals/MemberModal';
import Loader from 'react-loader-spinner'
import axios from 'axios';
import { URL } from "../config";

const MemberPage = props => {
  const [loading, setLoading] = useState(true);

  const [membersData, setMembersData] = useState({
    token: props.getCurrentUser.token,
    members: []
  });

  const { token, members } = membersData;
  // console.log('Members Page', token);

  const config = {
    headers: {
      Authorization: `Bearer ${token}`
    }
  };
  // console.log('config global', config);

  //GET ALL MEMBERS
  const getMembers = async (field = '') => {
    // console.log('field', field);
    try {
      const res = await axios.get(
        `${URL}/members${field}`,
        config
      );

      setMembersData({
        ...membersData,
        members: res.data
      });
      return setLoading(false)
    } catch (e) {
      console.log(e.response);
    }
  };

  useEffect(() => {
    getMembers();
  }, [setMembersData]);

  //( SOFT ) DELETE A MEMBER
  const deleteMember = async id => {
    try {
      const res = await axios.delete(
        `${URL}/members/${id}`,
        config
      );

      console.log(res.data);
      //SWAL

      return getMembers();
    } catch (e) {
      console.log(e.response.data);
      //SWAL
    }
  };

  //============================UPDATE A MEMBER: MODAL
  /*
  1) Set up Modal Component
  - show modal
  - pass member to modal

  2) Populate teams (not applicable to other collections)

  3) Update member


  */
  const [modalData, setModalData] = useState({
    modal: false,
    member: {}
  });

  const { modal, member } = modalData;
  const toggle = async id => {
    // console.log(typeof id == 'string');
    setLoading(!loading)
    try {
      if (typeof id === 'string') {
        const res = await axios.get(
          `${URL}/members/${id}`,
          config
        );

        console.log(res.data);
        return setModalData({
          modal: !modal,
          member: res.data
        });
      }

      setModalData({
        ...modalData,
        modal: !modal
      });
    } catch (error) {
      console.log(error);
    }
  };

  //UPDATE A MEMBER: POPULATE TEAMS
  const [teams, setTeams] = useState([]);

  const getTeams = async () => {
    try {
      const res = await axios.get(`${URL}/teams`, config);
      // console.log(res);

      setTeams(res.data);
    } catch (e) {
      console.log(e);
    }
  };

  useEffect(() => {
    getTeams();
  }, [setTeams]);

  //UPDATE MEMBER: AS IN
  const updateMember = async (id, updates) => {
    try {
      config.headers['Content-Type'] = 'application/json';
      console.log('updateMember', config);

      //recreate config
      //signify because you can't pass object to URL
      const body = JSON.stringify(updates);

      const res = await axios.patch(
        `${URL}/members/${id}`,
        body,
        config
      );

      getMembers();
      //close modal
      setModalData({
        ...modalData,
        modal: !modal
      });
      //SWAL
    } catch (error) {
      console.log(error);
      //SWAL
    }
  };

  //CREATE A MEMBER (NA)

  useEffect(() => {
    if (modal) {
      return setLoading(false)
    }
  }, [modal])

  return (
    <Fragment>
      {loading ? (
        <Container>
          <Row>
            <Col id="loaderContainer">
              <div id="loader">
                <Loader
                  type="BallTriangle"
                  color="#00BFFF"
                  height={200}
                  width={200}
                />
              </div>

            </Col>
          </Row>
        </Container>
      ) : (
          <Container className='my-5' fluid={false}>
            <Row className='mb-3'>
              <Col>
                <h1>Members</h1>
              </Col>
            </Row>
            <Row>
              <Col lg='4' className='mb-5'>
                <MemberForm />
              </Col>
              <Col className=''>
                <div>
                  <Button
                    className='btn-sm border mr-1'
                    onClick={() => getMembers()}
                  >
                    Get All
              </Button>
                  <Button
                    className='btn-sm border mr-1'
                    onClick={() => getMembers('?isActive=true')}
                  >
                    Get Active
              </Button>
                  <Button
                    className='btn-sm border mr-1'
                    onClick={() => getMembers('?isActive=false')}
                  >
                    Get Inactive
              </Button>
                </div>
                <hr />
                <MemberTable
                  membersAttr={members}
                  deleteMember={deleteMember}
                  toggle={toggle}
                />
              </Col>
            </Row>
          </Container>
        )}
      <MemberModal
        modal={modal}
        toggle={toggle}
        member={member}
        teams={teams}
        updateMember={updateMember}
      />

    </Fragment>
  );
};

export default MemberPage;
