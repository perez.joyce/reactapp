import React, { useState, useEffect } from 'react';
import { Container, Row, Col } from 'reactstrap';
import ProfileForm from '../forms/ProfileForm';
import ProfileImgForm from '../forms/ProfileImgForm';
import axios from 'axios';
import { URL } from "../config";

/*
 1) COMPONENT
    - building blocks of any React app
    - splits UI into independent, reusable pieces and deal with each piece in isolation
    - JS class or function that accepts inputs (i.e., props) and returns JSX  that describes how a UI will appear.
    

  2) PROPS
    - arguments passed into React components via HTML attributes
*/

const ProfilePage = props => {
  // console.log('ProfilePage username props', props.getCurrentUser.username);
  // console.log('ProfilePage token props', props.getCurrentUser.token);

  const config = {
    headers: {
      Authorization: `Bearer ${props.token}`
    }
  };

  const url = `${URL}/members/me`;

  const [memberData, setMemberData] = useState({});

  const getProfile = async () => {
    try {
      const res = await axios.get(url, config);
      console.log('ProfilePage response', res);
      console.log(res.data);

      // setMemberData({
      //   firstName: res.data.firstName,
      //   lastName: res.data.lastName,
      //   username: res.data.username,
      //   position: res.data.position,
      //   age: res.data.age,
      //   email: res.data.email
      // });

      const member = res.data;

      setMemberData({
        _id: member._id,
        firstName: member.firstName,
        lastName: member.lastName,
        position: member.position,
        age: member.age,
        email: member.email,
        username: member.username
      });
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getProfile();
  }, []);

  const updateProfile = async updates => {
    config.headers['Content-Type'] = 'application/json'; //without this, string won't be converted to
    console.log('updates', updates);
    //delete _id
    delete updates._id;
    console.log('updates', updates);
    try {
      const body = JSON.stringify(updates);
      await axios.patch(url, body, config);

      getProfile();
    } catch (e) {
      console.log(e);
    }
  };

  //UPDATE
  const updateImage = async body => {
    try {
      await axios.post(`${URL}/members/upload`, body, config);

      //SWEET ALERT
      window.location.reload();
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <Container className='my-5' fluid={false}>
      <Row className='mb-3'>
        <Col>
          <h1>Profile Page</h1>
        </Col>
      </Row>
      <Row>
        <Col md='6'>
          <ProfileImgForm _id={memberData._id} updateImage={updateImage} />
        </Col>
        <Col md='6'>
          <ProfileForm
            member={memberData}
            setMemberData={setMemberData}
            updateProfile={updateProfile}
          />
        </Col>
      </Row>
    </Container>
  );
};

export default ProfilePage;
