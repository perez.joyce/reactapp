import axios from 'axios';
import { URL } from "../../config";

//GET PROFILE
const getProfile = async (uname, token, setMemberData) => {
  try {
    const config = {
      headers: {
        Authorization: `Bearer ${token}`
      }
    };

    const body = JSON.stringify({ username: uname });
    const res = await axios.get(`${URL}/members/me`, config);
    console.log('ProfileForm response', res);

    // const member = res.data;
    const {
      _id,
      firstName,
      lastName,
      username,
      position,
      age,
      email,
      teamId
    } = res.data;

    //return updated states
    return setMemberData({
      _id,
      firstName,
      lastName,
      username,
      position,
      age,
      email,
      teamId
    });
  } catch (error) {
    console.log(error);
  }
};

export { getProfile };
