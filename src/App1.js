//declare dependencies
//destructuring
import React, { useState } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import './style.css';

//main components
import AppNavbar from './partials/AppNavbar';
import MembersPage from './pages/MembersPage';
import TeamsPage from './pages/TeamsPage';
import TasksPage from './pages/TasksPage';
import ProfilePage from './pages/ProfilePage';
import LoginPage from './pages/LoginPage';
import SignupPage from './pages/SignupPage';
import MainPage from './pages/MainPage';
import NotFoundPage from './pages/NotFoundPage';
import ProtectedRoute from './auth/ProtectedRoute';


const App = () => {

  const [appData, setAppData ] = useState({
    username: localStorage.username,
    token: localStorage.token
  })

  const { username, token } = appData;
  // console.log(token)


  //create protectedRoute
  const Logout = props => {
    localStorage.clear();
    window.location = '/login';
  }

  console.log(username)
  const getCurrentUser = () => {
    return { username, token }
  }

  

  //Redefine Routes to pass in methods
  const Profile = props => (
    <ProfilePage 
      {...props}
      getCurrentUser={getCurrentUser}
    />
    )

 

  return (
    <BrowserRouter>
      <AppNavbar username={username} token={token}/>
      <Switch>
        <ProtectedRoute component={MembersPage} path='/members' username={username}/>
        <Route component={TeamsPage} path='/teams'/>
        <Route component={TasksPage} path='/tasks' />
        <ProtectedRoute component={Profile} path='/profile' username={username} getCurrentUser={getCurrentUser() }/>
        <Route component={LoginPage} path='/login' />
        <Route component={SignupPage} path='/signup' />
        <ProtectedRoute component={Logout} path='/logout' />
        <Route component={MainPage} exact path='/' />
        <Route component={NotFoundPage} path='*' />
      </Switch>
    </BrowserRouter>
    )
}


//render component
export default App
