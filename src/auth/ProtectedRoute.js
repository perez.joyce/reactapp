import React from 'react';
import { Route, Redirect } from 'react-router-dom';

/*
 Pass in the component as a prop
 Destructure props so that you are able to access individual props easily
 Assign prop.component to Component with capital C

 ...rest pertains to other props that were passed. we're copying them here via spread operator

render prop takes in a function that returns a react element and calls it instead of implemnt its own render logic
render prop is a technique for sharing code between react components
expects component declaration

*/
// const ProtectedRoute = ({page: Component, ...rest}) => {
// 	console.log(rest)
// 	return(

// 		<Route 
// 			{...rest} 
// 			render={
// 				localStorage.token ? (props) =>  { return <Component {...props} /> } : () => (<Redirect to="/login" />)
// 			}
// 		/>

// 		)
// }

const ProtectedRoute = ({component: Component, ...rest}) => {
	// console.log(rest)

	return <Route render={ (props) => ((localStorage.token) ? <Component {...props} /> : <Redirect to="/login"/>) }/>

}


export default ProtectedRoute;